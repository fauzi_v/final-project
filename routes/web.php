<?php

use App\Kategori;
use App\Topik;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['middleware' => ['auth']], function () {
  Route::get('/home', 'HomeController@index');
  Route::resource('/question', 'TopikController');
  Route::get('/contact', function () {
      return view('halaman.contact', [
        'kategoris' => Kategori::all(),
        'active' => 'Contact'
    ]);
  });

  Route::resource('/profile', 'ProfileController');
  Route::resource('/kategori', 'KategoriController');
  Route::resource('/komentar', 'KomentarController');

});


Auth::routes();
