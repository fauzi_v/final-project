<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table="profiles";
    protected $fillable=["umur","bio","alamat","notelp","user_id"];

    public function users()
    {
        return $this->belongsTo(User::class);
    }
    public function topik()
    {
        return $this->hasMany(Topik::class);
    }

    public function komentar()
    {
        return $this->hasMany('App\Komentar');
    }
}
