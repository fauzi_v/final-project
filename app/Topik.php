<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topik extends Model
{
    protected $table = 'topiks';
    protected $fillable = ["pertanyaan","kategori_id"];
    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function kategori()
    {
        return $this->belongsTo(Kategori::class);
    }
    public function komentar()
    {
        return $this->hasMany(Komentar::class);
    }

    // public function scopeFilter($query, array $filters)
    // {
    //     $query->when($filters['search'] ?? false, function($query, $search){
    //         return $query->where('pertanyaan', 'like', '%' . $search .'%');
    //     });
    //     $query->when($filters['kategori'] ?? false, function($query, $kategori)
    //     {
    //         return $query->whereHas('kategori', function($query) use ($kategori){
    //             $query->where('id', $kategori);
    //         });
    //     });
    // }
}
