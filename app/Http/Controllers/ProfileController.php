<?php

namespace App\Http\Controllers;

use App\Topik;
use App\Profile;
Use File;
use App\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Profile $profile)
    {
      return view('profile.index', [
        'kategoris' => Kategori::all(),
        'active' => 'Profile',
        'topiks' => Topik::where('user_id', auth()->user()->id)->get()

      ]);

      $id = Auth::id();
      $profile = Profile::where('user_id', $id)->first();
      return view('profile.edit', compact('profile'));
    }

    public function update(Request $request, $id){
        $request->validate(
            [
                'umur' => 'required',
                'bio' => 'required',
                'alamat' => 'required',
                'notelp' => 'required',
                'avatar' => 'mimes:jpeg,jpg,png|max:2200'

            ],
        );

        $profile = Profile::find($id);

        if($request->has('avatar')) {
            $path = "avatar/";
            File::delete($path . $profile->avatar);
            $gambar = $request->avatar;
            $new_gambar = time() . " - " . $gambar->getClientOriginalName();
            $gambar->move('avatar/', $new_gambar);

            $profile->avatar = $new_gambar;
        }

        $profile->umur = $request->umur;
        $profile->bio = $request->bio;
        $profile->alamat = $request->alamat;
        $profile->notelp = $request->notelp;

        $profile->save();
        return redirect('/profile');
    }
    }


    // /**
    //  * Show the form for creating a new resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function create()
    // {
    //     //
    // }

    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function store(Request $request)
    // {
    //     //
    // }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  \App\Profile  $profile
    //  * @return \Illuminate\Http\Response
    //  */
    // public function show(Profile $profile)
    // {
    //     //
    // }

    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param  \App\Profile  $profile
    //  * @return \Illuminate\Http\Response
    //  */
    // public function edit(Profile $profile)
    // {
    //     //
    // }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  \App\Profile  $profile
    //  * @return \Illuminate\Http\Response
    //  */
    // public function update(Request $request, Profile $profile)
    // {
    //     //
    // }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  \App\Profile  $profile
    //  * @return \Illuminate\Http\Response
    //  */
    // public function destroy(Profile $profile)
    // {
    //     //
    // }
