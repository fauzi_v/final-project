<?php

namespace App\Http\Controllers;

use App\Topik;
use App\Profile;
use App\Kategori;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class TopikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('halaman.add', [
          'kategoris' => Kategori::all(),
          'active' => 'Home'
      ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
            'pertanyaan' => 'required',
            'thumbnail' => 'required|mimes:jpeg,jpg,png|max:2200',
            'kategori_id' => 'required'
            ],
            [
            'pertanyaan.required' => 'Pertanyaan Harus diisi tidak boleh kosong',
            'thumbnail.required' => 'Thumbnail Harus diisi tidak boleh kosong',
            'thumbnail.mimes' => 'Thumbnail Hanya bisa format jpeg, jpg, png',
            'kategori_id.required' => 'Kategori Harus dipilih tidak boleh kosong',
            ]
        );
        $gambar = $request->thumbnail;
        $new_gambar = time() . ' - ' . $gambar->getClientOriginalName();

        $topik = new Topik;
        $topik->pertanyaan = $request->pertanyaan;
        $topik->thumbnail = $new_gambar;
        $topik->kategori_id = $request->kategori_id;
        $topik->profile_id = Auth::user()->profile->user_id;
        $topik->user_id = Auth::user()->id;
        $topik->save();

        $gambar->move('thumbnail/', $new_gambar);

        /*DB::table('topiks')->insert(
            [
                'pertanyaan' => $request['pertanyaan'],
                'kategori_id' => $request['kategori_id'],
                'profile_id' => Auth::user()->profile->user_id,
                'user_id' => Auth::user()->id
            ]
        );/*/
        return redirect('/home')->with('success', 'Anda telah berhasil menambah pertanyaan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Topik  $topik
     * @return \Illuminate\Http\Response
     */
    public function show(Topik $topik)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Topik  $topik
     * @return \Illuminate\Http\Response
     */
    public function edit(Topik $topik)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Topik  $topik
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Topik $topik)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Topik  $topik
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $topik = Topik::find($id);
      $topik->delete();
      return redirect('/home');
    }
}
