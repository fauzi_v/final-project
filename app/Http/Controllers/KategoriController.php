<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Kategori;
use App\Topik;
use Illuminate\Support\Facades\DB;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('kategori.index', [
            'kategoris' => Kategori::all(),
            'active' => 'Kategori'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kategori.create', [
            'kategoris' => Kategori::all(),
            'active' => 'Kategori'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            "nama" => "required"
          ],
          [
            'nama.required' => 'Nama kategori harus diisi!',
          ]);

          DB::table('kategoris')->insert(
            [
                'nama' => $request['nama']
            ]
        );
          return redirect('/home')->with('success', 'Anda telah berhasil menambah kategori');
      }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kategori = Kategori::findOrFail($id);

        return view('halaman.show', [
            'kategoris' => Kategori::all(),
            'topiks' => Topik::all(),
            'kategori' => $kategori,
            'active' => 'Home'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function edit(Kategori $kategori)
    {
        return view('kategori.edit', [
            'kategoris' => Kategori::all(),
            'kategori' => $kategori,
            'active' => 'Kategori'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kategori $kategori)
    {
        $request->validate(
            [
            'nama' => 'required'
            ],
            [
            'nama.required' => 'Nama Kategori harus diisi tidak boleh kosong'
            ]
        );
        DB::table('kategoris')->where('id',$kategori->id)->update(
            [
                'nama' => $request['nama']
            ]
        );
        return redirect('/home')->with('success', 'Anda telah berhasil merubah data kategori');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kategori $kategori)
    {
        DB::table('kategoris')->where('id', '=', $kategori->id)->delete();
        return redirect('/kategori')->with('success', 'Anda telah berhasil mendelete data kategori');
    }
}
