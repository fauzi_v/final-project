<?php

namespace App\Http\Controllers;

use App\Topik;


use App\Kategori;
use Illuminate\Http\Request;
use App\Kategori as AppKategori;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $topiks= Topik::latest();
        if(request('search')) {
            $topiks->where('pertanyaan', 'like', '%' . request('search') .'%')
            ->orWhere('user_id', 'like', '%' . request('search') .'%' );
        }
        return view('halaman.home', [
            'kategoris' => Kategori::all(),
            'active' => 'Home',
            'topiks' => $topiks->get()
        ]);
    }
    public function add()
    {
        return view('halaman.add', [
            'kategoris' => AppKategori::all()
        ]);
    }
}
