<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
  protected $table = 'komentars';
  protected $fillable = ['topik_id', 'profile_id', 'komentar'];

  public function profile()
  {
      return $this->belongsTo(Profile::class);
  }

  public function topik()
  {
      return $this->belongsTo(Topik::class);
  }
}
