<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>GASGUS</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <!-- Fonts -->
        <!-- Lato -->
        <link href='http://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!-- CSS -->

        <link rel="stylesheet" href="{{asset('layout/landing/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('layout/landing/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('layout/landing/css/owl.carousel.css')}}">
        <link rel="stylesheet" href="{{asset('layout/landing/css/animate.css')}}">
        <link rel="stylesheet" href="{{asset('layout/landing/css/main.css')}}">
        <!-- Responsive Stylesheet -->
        <link rel="stylesheet" href="{{asset('css/responsive.css')}}">
    </head>

    <body id="body">

	    <!--
	    Header start
	    ==================== -->
	    <section id="hero-area">
	        <div class="container">
	            <div class="row">
	                <div class="col-md-6">
	                    <div class="block">
	                        <h1 class="wow fadeInDown">Welcome to GASGUS!</h1>
	                        <p class="wow fadeInDown" data-wow-delay="0.3s">Situs Forum Tanya Jawab di Indonesia, terdapat berbagai kategori pembahasan </p>
	                        <div class="wow fadeInDown" data-wow-delay="0.3s">
	                        	<a class="btn btn-default btn-home" href="login" role="button">Get Started</a>
	                        </div>
	                    </div>
	                </div>

	            </div><!-- .row close -->
	        </div><!-- .container close -->
	    </section><!-- header close -->
        <!-- Js -->
        <script src="{{asset('layout/landing/js/vendor/modernizr-2.6.2.min.js')}}"></script>
        <script src="layout/landing/js/vendor/jquery-1.10.2.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
        <script src="{{asset('layout/landing/js/jquery.lwtCountdown-1.0.js')}}"></script>
        <script src="{{asset('layout/landing/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('layout/landing/js/owl.carousel.min.js')}}"></script>
        <script src="{{asset('layout/landing/js/jquery.validate.min.js')}}"></script>
        <script src="{{asset('layout/landing/js/jquery.form.js')}}"></script>
        <script src="{{asset('layout/landing/js/jquery.nav.js')}}"></script>
        <script src="{{asset('layout/landing/js/jquery.sticky.js')}}"></script>
        <script src="{{asset('layout/landing/js/plugins.js')}}"></script>
        <script src="{{asset('layout/landing/js/wow.min.js')}}"></script>
        <script src="{{asset('layout/landing/js/main.js')}}"></script>

    </body>
</html>
