@extends('layout.master')

@section('content')
<!-- Alert Sukses-->
@if(session()->has('success'))
<div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
   <h5><i class="icon fas fa-check"></i> Alert!</h5>
   {{ session('success') }}
</div>
@endif
    @forelse ($topiks as $item)
<div class="post">
  <div class="user-block">
    <img class="img-circle img-bordered-sm" src="{{asset('avatar/' . $item->profile->avatar)}}" alt="user image">
    <span class="username">
      <a class="text-success"> {{$item->user->name}} </a>
      <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
    </span>
    <span class="description"> Kategori {{$item->kategori->nama}}</span>
  </div>
  <!-- /.user-block -->
  <img src="{{asset('thumbnail/'. $item->thumbnail)}}" class"card-img-top" alt="gada">
  <br></br>
  <p>

    {{$item->pertanyaan}}

  </p>

  <p>
    <span class="float-right">
      <a href="komentar" class="link-black text-sm">
        <i class="far fa-comments mr-1"></i> Comments
      </a>
    </span>
  </p>

</div>
@empty
<p class="text-center">Tidak Ada Pertanyaan</p>
@endforelse
<!-- /.post -->

<!-- Post -->

<!-- /.post -->


@endsection
