@extends('layout.master')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-success">
        <div class="card-header">
          <h3 class="card-title">Add Question</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
        <form action="/question" method="POST" enctype="multipart/form-data">
            @csrf
          <div class="form-group">
            <label for="pertanyaan">Pertanyaan</label>
            <textarea id="pertanyaan" class="form-control @error('pertanyaan') is-invalid @enderror" rows="4" name="pertanyaan">{{ old('pertanyaan') }}</textarea>
          </div>
            @error('pertanyaan')
            <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>{{ $message }}</div>
            @enderror
            <div class="form-group">
              <label>Thumbnail</label>
              <input type="file" class="form-control" name="thumbnail">
            </div>
              @error('thumbnail')
              <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>{{ $message }}</div>
              @enderror
          <div class="form-group">
            <label for="kategori_id">Kategori</label>
            <select  class="form-control custom-select @error('kategori_id') is-invalid @enderror" name="kategori_id">
              <option selected disabled>Select one</option>
              @foreach ($kategoris as $kategori)
              @if (old('kategori_id') == $kategori->id)
              <option value="{{ $kategori->id }}" selected>{{ $kategori->nama }}</option>
              @else
              <option value="{{ $kategori->id }}">{{$kategori->nama}}</option>
              @endif
              @endforeach
            </select>
          </div>
          <br>
            @error('kategori_id')
            <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>{{ $message }}</div>
            @enderror
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>

  </div>
  <div class="row">
    <div class="col-12">
      <a href="/home" class="btn btn-secondary">Cancel</a>
      <input type="submit" value="Buat Pertanyaan" class="btn btn-success float-right">
    </div>
  </div>
</form>
    </section>
@endsection
