@extends('layout.master')

@section('content')
<!-- Alert Sukses-->
@if(session()->has('success'))
<div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
   <h5><i class="icon fas fa-check"></i> Alert!</h5>
   {{ session('success') }}
</div>
@endif
    @forelse ($topiks as $item)
<div class="post">
  <div class="user-block">
    <img class="img-circle img-bordered-sm" src="{{asset('avatar/' . $item->profile->avatar)}}" alt="user image">
    <span class="username">
      <a class="text-success"> {{$item->user->name}} </a>
      <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
    </span>
    <span class="description"> Kategori {{$item->kategori->nama}}</span>
  </div>
  <!-- /.user-block -->
  <img src="{{asset('thumbnail/'. $item->thumbnail)}}" class"card-img-top" alt="gada">
  <br></br>
  <p>

    {{$item->pertanyaan}}

  </p>


  <form action="/komentar" method="POST">
    @csrf
    <input type="hidden" value="{{$item->id}}" name="topik_id">
    <div class="form-group">
      <label>Komentar</label>
      <textarea type="text" name="komentar" class="form-control"></textarea>
    </div>
    @error ('komentar')
    <div class="alert danger-alert">{{ $message }}</div>
    @enderror
    <input type="submit" value="Komen">
</div>
@empty
<p class="text-center">Tidak Ada Pertanyaan</p>
@endforelse
<h1>List Komentar</h1>
@forelse ($topiks->komentar as $item)
  <div class="media">
    <div class="media-body">
      <h5 class="mt-0">Media Heading</h5>
      <p>{{$item->pertanyaan}}</p>
    </div>
  </div>
  @empty

  @endforelse
<!-- /.post -->

<!-- Post -->

<!-- /.post -->


@endsection
