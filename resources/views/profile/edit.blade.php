<div class="tab-pane" id="settings">
    <form class="form-horizontal" action="/profile/{{Auth::user()->profile->id}}" method="POST" enctype="multipart/form-data">
      @csrf
      @method('put')
      <div class="form-group row">
        <label for="inputName" class="col-sm-2 col-form-label">Name</label>
        <div class="col-sm-10">
          <input type="text" class="form-control"  placeholder="{{Auth::user()->name}}" disabled>
        </div>
      </div>
      <div class="form-group row">
        <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
        <div class="col-sm-10">
          <input type="email" class="form-control"  placeholder="{{Auth::user()->email}}" disabled>
        </div>
      </div>
      <div class="form-group row">
        <label for="inputName2" class="col-sm-2 col-form-label">Umur</label>
        <div class="col-sm-10">
          <input type="text" class="form-control"  value="{{Auth::user()->profile->umur}}" name="umur">
        </div>
      </div>
      <div class="form-group row">
        <label for="inputExperience" class="col-sm-2 col-form-label">Bio</label>
        <div class="col-sm-10">
          <textarea class="form-control" name="bio" >{{Auth::user()->profile->bio}} </textarea>
        </div>
      </div>
      <div class="form-group row">
        <label for="inputSkills" class="col-sm-2 col-form-label">Alamat</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="alamat" value="{{Auth::user()->profile->alamat}}">
        </div>
      </div>
      <div class="form-group row">
        <label for="inputTelp" class="col-sm-2 col-form-label">No Telp</label>
        <div class="col-sm-10">
          <input type="number" class="form-control" name="notelp" value="{{Auth::user()->profile->notelp}}">
        </div>
      </div>
    <div class="form-group row">
      <label for="foto" class="col-sm-2 col-form-label">Foto Profile</label>
      <div class="col-sm-10">
        <input type="file" class="form-control" name="avatar">
      </div>
    </div>
      <div class="form-group row">
        <div class="offset-sm-2 col-sm-10">
          <div class="checkbox">
          </div>
        </div>
      </div>
      <div class="form-group row">
        <div class="offset-sm-2 col-sm-10">
          <button type="submit" class="btn btn-danger">Submit</button>
        </div>
      </div>
    </form>
  </div>