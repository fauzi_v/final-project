@extends('layout.master')

@section('content')
    <!-- Main content -->
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-success card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                                    {{-- src="{{asset('layout/dist/img/user4-128x128.jpg')}}" --}}
                       src="{{asset('avatar/' . Auth::user()->profile->avatar)}}"
                       alt="User profile picture">
                </div>
                @auth
                <h3 class="profile-username text-center">
                {{ Auth::user()->name }}
                </h3>
                <p class="text-muted text-center">{{ Auth::user()->email }}</p>
                @endauth
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">About Me</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <strong><i class="fas fa-pencil-alt mr-1"></i> Age</strong>

                <p class="text-muted">
                  {{Auth::user()->profile->umur}}
                </p>

                <hr>

                <strong><i class="fas fa-map-marker-alt mr-1"></i> Address</strong>

                <p class="text-muted">
                  {{Auth::user()->profile->alamat}}

                </p>

                <hr>

                <hr>

                <strong><i class="far fa-file-alt mr-1"></i> Bio</strong>

                <p class="text-muted">
                  {{Auth::user()->profile->bio}}
                </p>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">My Question</a></li>
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Edit Profile</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    <!-- Post -->
                    @forelse ($topiks as $item)
                    <div class="post">
                      <div class="user-block">
                        <img class="img-circle img-bordered-sm" src="{{asset('avatar/' . Auth::user()->profile->avatar)}}" alt="user image">
                        <span class="username">
                          <a href="#" class="text-success">{{ $item->user->name }}</a>
                          <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                        </span>
                        <span class="description"> Kategori {{$item->kategori->nama}}</span>
                      </div>
                      <img src="{{asset('thumbnail/'. $item->thumbnail)}}" class"card-img-top" alt="gada">
                      <br></br>
                      <!-- /.user-block -->
                      <p>
                       {{ $item->pertanyaan }}
                      </p>

                      <p>
                        <form action="/question/{{$item->id}}" method="POST">
                            @csrf
                            @method('delete')
                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                        </form>
                        <span class="float-right">
                          <a href="#" class="link-black text-sm">
                            <i class="far fa-comments mr-1"></i> Comments
                          </a>
                        </span>
                      </p>

                      <input class="form-control form-control-sm" type="text" placeholder="Type a comment">
                    </div>

                    @empty
                    <p class="text-center">Tidak Ada Pertanyaan</p>
                    @endforelse

                    <!-- /.post -->

                    <!-- Post -->

                    <!-- /.post -->

                    <!-- Post -->

                    <!-- /.post -->
                  </div>

                  @include('profile.edit')
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->

@endsection
