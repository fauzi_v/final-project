@extends('layout.master')
@section('content')
@if(session()->has('success'))
<div class="alert alert-success alert-dismissible">
   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
   <h5><i class="icon fas fa-check"></i> Alert!</h5>
   {{ session('success') }}
</div>
@endif
<section class="content">
<div class="row">
  <div class="col-md-12">
    <div class="card card-success">
      <div class="card-header">
        <h3 class="card-title">Edit Kategori</h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
            <i class="fas fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
            <table class="table table-striped">
                <thead>
                <tr>
                <th>No</th>
                <th>Nama Kategori</th>
                <th>action</th>
                </tr>
                </thead>
                <tbody>
                    @forelse ($kategoris as $key => $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->nama }}</td>
                        <td>
                            <form action="/kategori/{{$item->id}}" method="POST">
                            <a href="/kategori/{{$item->id}}/edit" class="btn btn-success btn-sm">Edit</a>
                                @csrf
                                @method('delete')
                                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                            </form>
                        </td>
                    </tr>
                    @empty
                    <H1>Data Kosong</H1>
                    @endforelse
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
        <!-- /.card -->
  </div>
</div>
@endsection
