@extends('layout.master')

@section('content')
<section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-success">
          <div class="card-header">
            <h3 class="card-title">Edit Kategori</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
          <form action="/kategori/{{$kategori->id}}" method="POST" enctype="multipart/form-data">
            @method('PUT')
              @csrf
            <div class="form-group">
              <label for="nama">Nama Kategori</label>
              <input id="nama" class="form-control @error('nama') is-invalid @enderror" rows="4" name="nama" value="{{ old('nama', $kategori->nama) }}">
            </div>
              @error('nama')
              <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>{{ $message }}</div>
              @enderror
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>

    </div>
    <div class="row">
      <div class="col-12">
        <a href="/home" class="btn btn-secondary">Cancel</a>
        <input type="submit" value="Simpan" class="btn btn-success float-right">
      </div>
    </div>
  </form>
      </section>
@endsection
