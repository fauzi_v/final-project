<section class="hero">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="hero__categories">
                    <div class="hero__categories__all bg-success">
                        <i class="fa fa-bars"></i>
                        <span>Categories</span>
                    </div>
                    <ul>
                        @forelse ($kategoris as $kategori)
                        <li><a href="/kategori/{{$kategori->id}}">{{$kategori->nama}}</a></li>
                        @empty
                        <li>Data Kosong</li>
                        @endforelse
                    </ul>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="hero__search">
                    <div class="hero__search__form">
                        <form action="/home">
                            <input type="text" placeholder="What do yo u want to know?" name="search" class="form-control" value="{{request('search')}}">
                            <button type="submit" class="site-btn bg-success">SEARCH</button>
                        </form>
                    </div>
                </div>
                <div>
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</section>
