<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Ogani Template">
    <meta name="keywords" content="Ogani, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Forum</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">
    <!-- Logo Title -->
    <link rel="icon" href="{{asset('layout/img/icon.png')}}">
    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('layout/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/jquery-ui.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('layout/css/style.css')}}" type="text/css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="{{asset('layout/layoutplus/plugins/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('layout/layoutplus/dist/css/adminlte.min.css')}}">
</head>

<body>
  <div id="preloder">
      <div class="loader"></div>
  </div>
    <!-- Humberger Begin -->
    <div class="humberger__menu__overlay"></div>
    <!-- Kalau mau ngubah Nav dibagian sini -->
    @include('layout.nav')

    <!-- Hero Section Begin -->
    <!-- Kalau mau ngubah Kategori dibagian sini -->
    @include('layout.side')


    <!-- Footer Section Begin -->
    @include('layout.footer')
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="{{asset('layout/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('layout/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('layout/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('layout/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('layout/js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('layout/js/mixitup.min.js')}}"></script>
    <script src="{{asset('layout/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('layout/js/main.js')}}"></script>
    <script src="{{asset('layout/layoutplus/plugins/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('layout/layoutplus/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('layout/layoutplus/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('layout/layoutplus/dist/js/demo.js')}}"></script>



</body>

</html>
