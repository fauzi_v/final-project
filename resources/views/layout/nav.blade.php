<div class="humberger__menu__wrapper">
    <div class="humberger__menu__logo">
      <a href="/home"><img src="{{asset('layout/img/logo.png')}}" alt=""></a>
    </div>
    <div class="humberger__menu__widget">
        <a class="header__top__right__auth text-danger" href="{{ route('logout') }}"
           onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();"><i class="fa fa-user"></i>
            Logout
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </div>
    <nav class="humberger__menu__nav mobile-menu">
        <ul>
            <li><a href="/home">Home</a></li>
            <li><a>Category</a>
                <ul class="header__menu__dropdown">
                    <li class="{{ (request()->is('/kategori')) ? 'active' : ''}}"><a href="/kategori">Add Category</a></li>
                    <li class="{{ (request()->is('/kategori/edit')) ? 'active' : ''}}"><a href="/kategori/edit">Edit Category</a></li>
                </ul>
            </li>
            <li><a href="/profile">Profile</a></li>
            <li class="{{ (request()->is('/contact')) ? 'active' : ''}}"><a href="contact">Contact</a></li>
        </ul>
    </nav>
    <div id="mobile-menu-wrap"></div>
    <div class="humberger__menu__contact">
        <ul>
            <li><a href="/question/create" class="text-success"><i class="fa fa-user"></i> Add Question</a></li>
            <li><i class="fa fa-envelope"></i> forumtanyajawab@gmail.com</li>
            <li>Forum Tanya Jawab</li>
        </ul>
    </div>
</div>
<!-- Humberger End -->

<!-- Header Section Begin -->
<header class="header">
    <div class="header__top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="header__top__left">
                        <ul>
                            <li><i class="fa fa-envelope"></i> forumtanyajawab@gmail.com</li>
                            <li>Forum Tanya Jawab</li>
                            <li><a href="/question/create" class="text-success"><i class="fa fa-user"></i> Add Question</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="header__top__right">
                      <a class="header__top__right__auth text-danger" href="{{ route('logout') }}"
                         onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();"><i class="fa fa-user"></i>
                          Logout
                      </a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
          <div class="col-lg-3">
              <div class="header__logo">
                  <a href="/home"><img src="{{asset('layout/img/logo.png')}}" alt=""></a>
              </div>
          </div>
          <div class="col-lg-6">
            <div class="row-centered">
              <nav class="header__menu">
                  <ul>
                      <li class="{{ ($active === "Home") ? 'active' : ''}}"><a href="/home" >Home</a></li>
                      <li class="{{ ($active === "Kategori") ? 'active' : ''}}"><a>Category</a>
                          <ul class="header__menu__dropdown">
                            <li><a href="/kategori/create">Add Category</a></li>
                            <li><a href="/kategori">Edit Category</a></li>
                          </ul>
                      </li>
                      <li class="{{ ($active === "Profile") ? 'active' : ''}}"><a href="/profile">Profile</a></li>
                      <li class="{{ ($active === "Contact") ? 'active' : ''}}"><a href="/contact">Contact</a></li>
                  </ul>
                </nav>
              </div>
            </div>
          <div class="col-lg-3">
            <div class="header__cart">
              <div class="header__cart__price"><span>Diskusikan Dimanapun, kapanpun!</span></div>
            </div>
          </div>
        </div>
        <div class="humberger__open">
          <i class="fa fa-bars"></i>
        </div>
      </div>
</header>

<!-- Header Section End -->
