<?php

use App\Kategori as AppKategori;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        AppKategori::create([
            'nama' => 'Lifestyle'
        ]);
        AppKategori::create([
            'nama' => 'Hobby'
        ]);
        AppKategori::create([
            'nama' => 'Game'
        ]);
        AppKategori::create([
            'nama' => 'Sport'
        ]);
        AppKategori::create([
            'nama' => 'Entertainment'
        ]);
        AppKategori::create([
            'nama' => 'Tech'
        ]);
        AppKategori::create([
            'nama' => 'Food And Travel'
        ]);
        AppKategori::create([
            'nama' => 'Automotive'
        ]);
        AppKategori::create([
            'nama' => 'Politics'
        ]);
    }
}
